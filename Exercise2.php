<?php

// Create connection

$link = mysqli_connect("localhost", "kristjanmikk", "123456789", "patient_information");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}


$sql = "SELECT patient.pn, patient.lastName, patient.firstName, 
insurance.iname, insurance.from_date, 
insurance.to_date FROM insurance inner join patient on 
insurance.patient_id = patient._id order by insurance.from_date asc, patient.lastName asc";

$result = mysqli_query($link, $sql);

$unique = array();


while($row = mysqli_fetch_assoc($result)) {

    $l = mb_strlen($row['lastName'], 'UTF-8');
    for($i = 0; $i < $l; $i++) {
        $char = mb_substr($row['lastName'], $i, 1, 'UTF-8');
        if(!array_key_exists($char, $unique))
            $unique[$char] = 0;
        $unique[$char]++;
    }

    $f = mb_strlen($row['firstName'], 'UTF-8');
    for($i = 0; $i < $f; $i++) {
        $char = mb_substr($row['firstName'], $i, 1, 'UTF-8');
        if(!array_key_exists($char, $unique))
            $unique[$char] = 0;
        $unique[$char]++;
    }

    $letterCount = $l + $f;
    $count = $letterCount + $count;

    $fromDate = $row['from_date'];
    $newFromDate = date("m-d-y", strtotime($fromDate));
    $toDate = $row['to_date'];
    $newToDate = date("m-d-y", strtotime($toDate));

    echo "{$row['pn']}, ".
       "{$row['lastName']}, ".
       "{$row['firstName']}, ".
       "{$row['iname']}, ".
       $newFromDate.", ". 
       $newToDate." ".
       "\n";
 }

 uksort($unique, "strcasecmp");

  foreach($unique as $letter => $amount){
    $frequency = $amount / $count * 100;
    $formatNumber = number_format($frequency, 2);
    echo "$letter - $amount - $formatNumber%" . "\n";
}
?>


