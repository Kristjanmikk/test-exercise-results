<?php

$link = mysqli_connect("localhost", "kristjanmikk", "123456789", "patient_information");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

interface PatientRecord {
    public function getId();
    public function getPn();
  }

  class Patient implements PatientRecord {

      public $id;
      public $pn;
      public $firstName;
      public $lastName;
      public $dob;
      public $insuranceRecords;

      public function __construct($pn, $link) { 

        $sql = "SELECT * FROM patient WHERE pn = $pn";
        $result = mysqli_query($link, $sql);

        $row = mysqli_fetch_assoc($result);

        $formatDob = $row['dob'];
        $newDob = date("m-d-y", strtotime($formatDob));

            $this-> id = $row['_id'];
            $this-> pn = $pn;
            $this-> firstName = $row['firstName'];
            $this-> lastName = $row['lastName'];
            $this-> dob = $newDob;
      } 

    public function getId() {
        return $this -> id;
    }

    public function getPn() {
        return $this -> pn;
    }

    public function fullName() {
        $name = " {$this -> firstName} {$this -> lastName}";
        return $name;
    }

    public function getRecords($link, $curDate) {

        $sql = "SELECT patient.pn,  
        insurance.iname, insurance._id FROM insurance inner join patient on 
        insurance.patient_id = patient._id where patient_id = {$this -> id}";

        $result = mysqli_query($link, $sql);

        $x = 1;

        while($row = mysqli_fetch_assoc($result)) {

            $rowNumber['type'] = $x++;
            ${'record' . $rowNumber['type']} = new Insurance ("{$row['_id']}", $link);

            echo 
            "{$row['pn']}, ".
            "{$row['iname']}, ".
            " insurance_id:{$row['_id']}, ".
            "{$this -> fullName()}".
            ${'record' . $rowNumber['type']} ->insuranceIsValid($curDate).
            "\n";
        }

    }

  }

  class Insurance implements PatientRecord {

    public $id;
    public $pn;
    public $iname;
    public $fromDate;
    public $toDate;

    public function __construct($id, $link) { 

        $sql = "SELECT * FROM insurance WHERE _id = $id";
        $result = mysqli_query($link, $sql);

        $row = mysqli_fetch_assoc($result);

        $formatFromDate = $row['from_date'];
        $newFromDate = date("m-d-y", strtotime($formatFromDate));

        $formatToDate = $row['to_date'];
        $newToDate = date("m-d-y", strtotime($formatToDate));

            $this-> id = $id;
            $this-> pn = $row['patient_id'];
            $this-> iname = $row['iname'];
            $this-> fromDate = $newFromDate;
            $this-> toDate = $newToDate;

      }

    public function getId() {
        return $this -> id;
    }

    public function getPn() {
        return $this -> pn;
    }

    public function insuranceIsValid($date) {
        $newDate = date("m-d-y", strtotime($date));
        if (($newDate >= $this-> fromDate) && ($newDate <= $this-> toDate)){
            echo "YES, ";
        } elseif (($newDate >= $this-> fromDate) && ($this-> toDate === null)) {
            echo "YES, ";
        }else{
            echo "NO, ";  
        }
    }
  }


    function getAllRecords($link, $date){
        $patient1 = new Patient ("00000000001", $link);
        $patient1->getRecords($link, $date);
        $patient2 = new Patient ("00000000002", $link);
        $patient2->getRecords($link, $date);
        $patient3 = new Patient ("00000000003", $link);
        $patient3->getRecords($link, $date);
        $patient4= new Patient ("00000000004", $link);
        $patient4->getRecords($link, $date);
        $patient5 = new Patient ("00000000005", $link);
        $patient5->getRecords($link, $date);
    }

    getAllRecords($link, date("m-d-y"));


?>