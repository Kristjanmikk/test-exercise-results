-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 09, 2021 at 01:10 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `patient_information`
--

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE `insurance` (
  `_id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(10) UNSIGNED NOT NULL,
  `iname` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`_id`, `patient_id`, `iname`, `from_date`, `to_date`) VALUES
(1, 1, 'medicare', '2019-07-05', '2019-07-08'),
(2, 1, 'medicare', '2021-06-05', '2021-06-08'),
(3, 2, 'medicare', '2020-06-05', '2020-06-08'),
(4, 2, 'medicare', '2021-01-05', '2021-06-08'),
(5, 3, 'medicaid', '2021-01-22', '2021-02-26'),
(6, 3, 'medicaid', '2015-11-22', '2015-11-26'),
(7, 4, 'blue cross', '2021-03-05', '2021-04-04'),
(8, 4, 'blue cross', '2021-07-22', '2021-08-26'),
(9, 5, 'medicaid', '2021-01-01', '2021-01-10'),
(10, 5, 'medicaid', '2017-01-22', '2017-02-26');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `_id` int(10) UNSIGNED NOT NULL,
  `pn` varchar(11) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `firstName` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lastName` varchar(25) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `dob` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`_id`, `pn`, `firstName`, `lastName`, `dob`) VALUES
(1, '00000000001', 'mathew', 'smith', '1975-12-21'),
(2, '00000000002', 'jason', 'dell', '1995-06-26'),
(3, '00000000003', 'joe', 'biden', '1942-11-20'),
(4, '00000000004', 'hasan', 'piker', '1983-01-05'),
(5, '00000000005', 'matt', 'damon', '1975-06-15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `insurance`
--
ALTER TABLE `insurance`
  ADD CONSTRAINT `insurance_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
